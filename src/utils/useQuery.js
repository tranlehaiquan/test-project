import { useLocation } from 'react-router-dom';

/**
 * @returns {URLSearchParams}
 */
export default function useQuery() {
  return new URLSearchParams(useLocation().search);
}
