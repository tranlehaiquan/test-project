import { useSelector } from 'react-redux';

const useUser = () => {
  const { user } = useSelector((state) => state.users);
  return user;
};

export default useUser;
