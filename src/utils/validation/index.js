import * as yup from 'yup';
import pick from 'lodash/pick';

export const validations = {
  password: yup
    .string()
    .required('Password is required!')
    .min(5, 'Password Too Short!')
    .max(50, 'Password Too Long'),
  email: yup.string().email('Invalid email').required('Email is required!'),
  code: yup.string().required('Code is required!'),
  passwordConfirm: yup
    .string()
    .required('Confirm password is required')
    .when('password', {
      is: (val) => !!(val && val.length > 0),
      then: yup
        .string()
        .oneOf([yup.ref('password')], 'Both password need to be the same'),
    }),
};

const extractFields = (fields) => pick(validations, fields);

/**
 *
 * @param {string[]} values
 */
const getValidationSchema = (values) =>
  yup.object().shape(extractFields(values));

export const generateValidationFromSchema = (shape) =>
  yup.object().shape(shape);
export default getValidationSchema;
