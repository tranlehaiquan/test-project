/**
 * Get Browser id
 */
const getBrowserId = () => {
  let guid = '';
  guid += window.navigator.userAgent.replace(/\D+/g, '');
  guid += window.navigator.plugins.length;
  guid += window.screen.height || '';
  guid += window.screen.width || '';
  guid += window.screen.pixelDepth || '';

  return guid;
};

export default getBrowserId;
