import React from 'react';

import Layout from 'components/Layout';
import Container from 'components/Container';
import { Login as LoginForm } from 'components/IdentityForm';
import Grid from '@material-ui/core/Grid';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(({ palette, spacing }) => ({
  root: {},
  loginForm: {
    margin: spacing(4, 0),
    backgroundColor: palette.common.white,
    padding: spacing(7, 5),
  },
}));

export default function Login() {
  const classes = useStyles();
  return (
    <Layout>
      <Container>
        <Grid container justify="center">
          <Grid item md={6} xs={12}>
            <div className={classes.loginForm}>
              <LoginForm />
            </div>
          </Grid>
        </Grid>
      </Container>
    </Layout>
  );
}
