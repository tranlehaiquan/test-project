import React from 'react';

import Masonry from 'react-masonry-css';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PlayCircleOutline from '@material-ui/icons/PlayCircleOutline';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Container from '../../components/Container';

const useStyles = makeStyles(({ spacing }) => ({
  root: {
    margin: spacing(2, 0),
  },
  masonryGrid: {
    display: 'flex',
    marginLeft: -10 /* gutter size offset */,
    width: 'auto',
    maxHeight: 'calc(100vh - 315px)',
    overflow: 'auto',
  },
  masonryGridColumn: {
    paddingLeft: 10 /* gutter size */,
    backgroundClip: 'padding-box',

    '& > div': {
      marginBottom: 10,
    },
    '& img': {
      maxWidth: '100%',
    },
  },
  media: {
    maxWidth: '100%',
  },
  cardContent: {
    padding: 0,
  },
  cardActions: {
    padding: 0,
    justifyContent: 'center',
  },
}));

export default function Stars(props) {
  const { stars } = props;
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container disableGutters>
        <Masonry
          breakpointCols={3}
          className={classes.masonryGrid}
          columnClassName={classes.masonryGridColumn}
        >
          {stars.map((star) => (
            <Card variant="outlined">
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    R
                  </Avatar>
                }
                action={
                  <IconButton aria-label="settings">
                    <MoreVertIcon />
                  </IconButton>
                }
                title={`${star.firstname} ${star.lastname}`}
                subheader={star.country}
              />
              <CardContent className={classes.cardContent}>
                <img src={star.profilephoto} alt={star.firstname} />
              </CardContent>
              <CardActions className={classes.cardActions} disableSpacing>
                <IconButton aria-label="add to favorites">
                  <FavoriteIcon />
                </IconButton>
                <IconButton aria-label="share">
                  <PlayCircleOutline />
                </IconButton>
              </CardActions>
            </Card>
          ))}
        </Masonry>
      </Container>
    </div>
  );
}
