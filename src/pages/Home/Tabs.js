import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import StarBorderOutlinedIcon from '@material-ui/icons/StarBorderOutlined';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
});

export default function Tabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      fullWidth
      className={classes.root}
    >
      <BottomNavigationAction label="All" icon={<StarBorderOutlinedIcon />} />
      <BottomNavigationAction
        label="Athletes"
        icon={<StarBorderOutlinedIcon />}
      />
      <BottomNavigationAction
        label="Actors"
        icon={<StarBorderOutlinedIcon />}
      />
      <BottomNavigationAction
        label="Comedians"
        icon={<StarBorderOutlinedIcon />}
      />
    </BottomNavigation>
  );
}
