import React, { useState, useEffect } from 'react';
import api from 'services/api';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typo from '@material-ui/core/Typography';

import Layout from 'components/Layout';
import Tabs from './Tabs';
import Stars from './Stars';

export default function Home() {
  const [stars, setStars] = useState([]);
  const [pending, setPending] = useState(false);

  async function fetchData() {
    setPending(true);
    const starsBE = await api.getAllStars();
    setStars(starsBE.stars);
    setPending(false);
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Layout>
      <Tabs />
      <Box
        pt={2}
        pb={2}
        display={pending ? 'flex' : 'none'}
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
      >
        <CircularProgress /> <Typo>Loading...</Typo>
      </Box>
      <Box hidden={pending}>
        {/* <div className={classes.heroContent}> */}
        <Stars stars={stars} />
        {/* </div> */}
      </Box>
    </Layout>
  );
}
