import React from 'react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { PersistGate } from 'redux-persist/integration/react';
import { SnackbarProvider } from 'notistack';

import { store, persistor } from './store/configureStore';
import { SnackbarUtilsConfigurator } from 'components/SnackbarUtilsConfigurator';
import theme from './utils/theme';
import Routers from './routers';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SnackbarProvider maxSnack={3} autoHideDuration={2000}>
            <SnackbarUtilsConfigurator />
            <CssBaseline />
            <Routers />
          </SnackbarProvider>
        </PersistGate>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
