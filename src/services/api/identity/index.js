import { postNonAuth, post } from 'services/api';
import getBrowserId from 'utils/getBrowserId';

const browserId = getBrowserId();
/**
 * @param {email: string, password: string} userInfo
 */
export function login(userInfo) {
  return postNonAuth(`auth/user/signin`, { ...userInfo, browserId });
}

export function logout() {
  return post(`auth/user/logout`);
}

/**
 * @param {email: string, password: string} userInfo
 */
export function register(userInfo) {
  return postNonAuth(`auth/user/signup`, { ...userInfo, browserId });
}

/**
 * @param {string} email
 * @return {Promise<any>}
 */
export function forgotPassword(email) {
  return postNonAuth(`auth/user/forgotpassword`, { email });
}

/**
 *  @param {code: string, email: string} userInfo
 * @return {Promise<any>}
 */
export function verifyCodeForgotPwd(userInfo) {
  return postNonAuth(`auth/user/varifyotp`, userInfo);
}

/**
 *  @param {code: string, email: string, password: string} userInfo
 * @return {Promise<any>}
 */
export function resetPasswordForgotten(userInfo) {
  return postNonAuth(`auth/user/changepassword`, userInfo);
}
