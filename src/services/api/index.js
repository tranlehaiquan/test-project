// @flow

import {
  get,
  getBlob,
  post,
  put,
  del,
  getNonAuth,
  postNonAuth,
} from './core/methods';
import authRequest from './core/auth-request';
import request from './core/request-with-retry';
import * as starsEndpoints from './stars';
import * as identityEndpoints from './identity';

import {
  ServerError,
  BadRequestError,
  UnauthorisedError,
  UnknownError,
  ConcurrencyError,
  ForbiddenError,
  GoneError,
} from './core/errors';

export {
  // Core api utils
  get,
  getBlob,
  getNonAuth,
  post,
  postNonAuth,
  put,
  del,
  authRequest,
  request,
};

export {
  ServerError,
  BadRequestError,
  UnauthorisedError,
  UnknownError,
  ConcurrencyError,
  ForbiddenError,
  GoneError,
};

export default {
  // Api endpoints
  ...starsEndpoints,
  ...identityEndpoints,
};
