// @flow

import { ConcurrencyError } from './errors';
import request from './request';

import type { Request } from './types';

const MAX_ATTEMPTS = 3; // Original request plus 2 retries

async function requestWithRetry(props: Request): Promise<Object> {
  const sendRequest = async attempt => {
    try {
      // Send the request and wait for the response.
      // If it's successful then just return it's response to the calling code.
      return await request(props);
    } catch (error) {
      // If an error occurs then check if we should retry.
      if (error instanceof ConcurrencyError && attempt < MAX_ATTEMPTS) {
        // Try the request again.
        return sendRequest(attempt + 1);
      }
      // If we're not retrying then just return the error to the calling code.
      throw error;
    }
  };

  return sendRequest(1);
}

export default requestWithRetry;
