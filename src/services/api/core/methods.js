// @flow

import { queryString } from 'lib/url';
import authRequest from './auth-request';
import requestWithRetry from './request-with-retry';

// GET requests don't support a "body" so we convert the body into a query string.
const get = (url: string, body?: Object, baseUrl?: string): Promise<*> => {
  const urlWithQueryString = body ? [url, queryString(body)].join('') : url;
  return authRequest({ url: urlWithQueryString, method: 'GET', baseUrl });
};

// GET requests don't support a "body" so we convert the body into a query string.
const getNonAuth = (url: string, body?: Object) => {
  const urlWithQueryString = body ? [url, queryString(body)].join('') : url;
  return requestWithRetry({ url: urlWithQueryString, method: 'GET' });
};

const getBlob = (url: string, body?: Object) => {
  const urlWithQueryString = body ? [url, queryString(body)].join('') : url;
  return authRequest({
    url: urlWithQueryString,
    method: 'GET',
    responseType: 'blob',
  });
};

const post = (
  url: string,
  body: Object | Array<Object>,
  baseUrl?: string,
): Promise<*> => authRequest({ url, method: 'POST', body, baseUrl });

const postNonAuth = (
  url: string,
  body: Object | Array<Object>,
  baseUrl?: string,
): Promise<*> => requestWithRetry({ url, method: 'POST', body, baseUrl });

const put = (url: string, body: Object | Array<Object>) =>
  authRequest({ url, method: 'PUT', body });

const del = (url: string) => authRequest({ url, method: 'DELETE' });

export { get, getBlob, post, put, del, getNonAuth, postNonAuth };
