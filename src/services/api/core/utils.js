// @flow

const getFileNameFromResponseHeaders = (headers: Headers): string => {
  let fileName = '';
  const disposition = headers.get('content-disposition');
  if (disposition && disposition.indexOf('filename') !== -1) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(disposition);
    if (matches != null && matches[1]) {
      fileName = matches[1].replace(/['"]/g, '');
    }
  }

  return fileName;
};

export default {
  getFileNameFromResponseHeaders,
};
