// @flow

import request from './request-with-retry';
/**
 * @Bug: CIRCULAR_DEPENDENCY
 * TODO This file is one that causes circular dependencies
 * This is because we import store directly and dispatch an action
 * outside of the redux context.
 */
// import store from 'app/redux/store';
// import { actions } from 'app/redux/auth/exports';
// import { getSessionToken } from 'services/session';
import { GoneError, UnauthorisedError } from './errors';
// import type { RequestMethod, Request } from './types';
import { store } from 'store/configureStore';

type AuthRequestParams = {|
  url: string,
  method: RequestMethod,
  body?: Object | Array<Object>,
  baseUrl?: string,
  responseType?: string,
|};

const authRequest = ({
  url,
  method,
  body,
  baseUrl,
  responseType = 'json',
}: AuthRequestParams): Promise<*> => {
  const token = store.getState().users.token;
  const options = {
    headers: { authorization: token && `Bearer ${token}` },
    responseType,
  };
  const req = {
    url,
    method,
    body,
    baseUrl,
    options,
  };

  return request(req).catch((error) => {
    // If an UnauthorisedError is received we assume it means the user's session has expired.
    const isUnauthorised = error instanceof UnauthorisedError;
    // If a GoneError is received it means we have tried to access an "expired" endpoint.
    const isGone = error instanceof GoneError;
    if (isUnauthorised || isGone) {
      // store.dispatch(actions.unauthorisedAccess());
    }

    return Promise.reject(error);
  });
};

export default authRequest;
