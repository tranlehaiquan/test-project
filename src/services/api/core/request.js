// @flow

/**
 * HTTP request utility for internal APIs
 *
 * This utility was designed for consumption specifcally with Scorecards API
 * services owned by Ansarada (e.g gateway.dev1.readiness.ansarada.com).
 *
 * This isn't intended to be used for making HTTP requests to external APIs.
 * See app/services/request if you just need a utility for making standard http requests
 */
import config from 'config';
import util from './utils';
// import 'isomorphic-fetch';

import {
  ServerError,
  BadRequestError,
  UnknownError,
  UnauthorisedError,
  ConcurrencyError,
  ForbiddenError,
  GoneError,
} from './errors';

import type { BlobData, Request, SuccessResponseData } from './types';

const composeUrl = (base: string) => (endpoint: string) => `${base}${endpoint}`;

const JSON_MIME_TYPE_WITH_ENCODING = 'application/json; charset=utf-8';

async function handleBlob(res: Response): Promise<BlobData> {
  if (res.status === 200) {
    const fileName = util.getFileNameFromResponseHeaders(res.headers);

    if (fileName) {
      const responseData = await res.blob();
      const blobData: BlobData = {
        fileName,
        responseData,
      };

      return blobData;
    }
  }
  throw new Error('Invalid server error response body');
}

async function request<T>({
  url,
  method,
  body,
  baseUrl,
  options,
}: Request): Promise<SuccessResponseData<T>> {
  const fullUrl: string = composeUrl(baseUrl || config.apiUrl)(url);

  const opts = options || {};
  const headers = {
    accept: JSON_MIME_TYPE_WITH_ENCODING,
    'content-type': JSON_MIME_TYPE_WITH_ENCODING,
    ...opts.headers,
  };

  const fetchOptions = {
    ...options,
    method,
    body: body && JSON.stringify(body),
    headers,
  };

  const res = await fetch(fullUrl, fetchOptions);

  // handle empty responses
  if (res.status === 204) {
    return {};
  }

  if (options && options.responseType === 'blob') {
    return handleBlob(res);
  }

  // Parse data on the response body.
  const responseData: Object | Array<Object> = await res.json();

  // If successful, then return the data.
  if (res.status >= 200 && res.status < 300) {
    const successData: SuccessResponseData<T> = responseData;

    /**
     * return reject for
     * action handle
     */
    if (!successData.success) {
      return Promise.reject(successData);
    }

    return successData;
  }

  // Otherwise handle an error response.

  // Flow requires that we have an object here rather than an array, so add a check for that.
  // This condition should never actually occur because our API should alway send an object
  // for the error body data.
  if (Array.isArray(responseData)) {
    throw new Error('Invalid server error response body');
  }

  // Server Errors

  if (res.status >= 500) {
    throw new ServerError(responseData);
  }

  // Client Errors

  if (res.status === 400) {
    throw new BadRequestError(responseData);
  }

  if (res.status === 401) {
    throw new UnauthorisedError(responseData);
  }

  if (res.status === 403) {
    throw new ForbiddenError(responseData);
  }

  if (res.status === 409) {
    throw new ConcurrencyError(responseData);
  }

  if (res.status === 410) {
    throw new GoneError(responseData);
  }

  throw new UnknownError(responseData);
}

export { composeUrl }; // For testing
export default request;
