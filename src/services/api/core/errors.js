// @flow

import type { ApiErrorCode } from './types';

class ApiError {
  errors: Array<string>;
  errorCode: ApiErrorCode;
  constructor({
    errors,
    errorCode,
  }: {
    errors: Array<string>,
    errorCode: ApiErrorCode,
  }) {
    this.errors = errors;
    this.errorCode = errorCode;
  }
}

class ServerError extends ApiError {}
class BadRequestError extends ApiError {}
class UnauthorisedError extends ApiError {}
class UnknownError extends ApiError {}
class ConcurrencyError extends ApiError {}
class ForbiddenError extends ApiError {}
class GoneError extends ApiError {}

export {
  ServerError,
  BadRequestError,
  UnauthorisedError,
  UnknownError,
  ConcurrencyError,
  ForbiddenError,
  GoneError,
};
