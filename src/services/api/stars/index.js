import { postNonAuth } from 'services/api';

// public
export function getTopTenStars(query) {
  const params =
    query |
    {
      limit: 10,
    };
  return postNonAuth(`open/get_toptenstars/D5C5FADA6B724`, params);
}

export function getAllStars() {
  return postNonAuth(`open/get_stars/51CC4124FDE72`, {});
}
