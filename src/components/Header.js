import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useUser from 'utils/useUser';
import Link from 'components/Link';
import MuiLink from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import { UserLogged } from 'components/UserLogged';
import IconButton from '@material-ui/core/IconButton';
import SupervisedUserCircleOutlinedIcon from '@material-ui/icons/SupervisedUserCircleOutlined';
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';
import NotificationsOutlinedIcon from '@material-ui/icons/NotificationsOutlined';
import Tooltip from '@material-ui/core/Tooltip';

import logo from 'assets/logo@2x.png';
import logoWebp from 'assets/logo@2x.webp';
import Container from './Container';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#fff',
  },
  logo: {
    '& img': {
      height: 40,
    },
  },
  toolbar: {
    padding: theme.spacing(2, 0),
    display: 'flex',
    justifyContent: 'space-between',
  },
  button: {
    marginLeft: theme.spacing(1),
  },
}));

export default function Header() {
  const classes = useStyles();
  const user = useUser();

  return (
    <div className={classes.root}>
      <AppBar position="static" color="inherit" elevation={0}>
        <Container>
          <Toolbar className={classes.toolbar}>
            <Link to="/">
              <div className={classes.title}>
                <picture className={classes.logo} alt="logo">
                  <source srcSet={logoWebp} type="image/webp" />
                  <source srcSet={logo} type="image/png" />
                  <img src={logo} alt="Header logo" />
                </picture>
              </div>
            </Link>
            <Box>
              <IconButton aria-label="Sign in" className={classes.margin}>
                <NotificationsOutlinedIcon fontSize="medium" />
              </IconButton>
              {user && <UserLogged />}
              {!user && (
                <>
                  <MuiLink component={Link} to="/login" color="inherit">
                    <IconButton aria-label="Sign in" className={classes.margin}>
                      <Tooltip title="Sign in">
                        <SupervisedUserCircleOutlinedIcon fontSize="large" />
                      </Tooltip>
                    </IconButton>
                  </MuiLink>

                  <MuiLink component={Link} to="/register" color="inherit">
                    <IconButton label="Sign up" className={classes.margin}>
                      <Tooltip title="Sign up">
                        <PersonAddOutlinedIcon fontSize="large" />
                      </Tooltip>
                    </IconButton>
                  </MuiLink>
                </>
              )}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </div>
  );
}
