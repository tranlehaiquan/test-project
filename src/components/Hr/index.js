import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import cls from 'clsx';

const useStyles = makeStyles(({ spacing }) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: spacing(1),
  },
  score: {
    flex: 1,
    height: '100%',
    '& hr': {
      display: 'block',
      height: 1,
      border: 0,
      borderTop: '1px solid #E0E0E0',
      padding: 0,
    },
  },
  content: {
    display: 'inline-block',
    paddingLeft: spacing(1),
    paddingRight: spacing(1),
  },
}));

interface Props {
  content: string;
  className?: string;
}

const Hr: React.FunctionComponent<Props> = ({ content, className }) => {
  const classes = useStyles();

  return (
    <div className={cls(classes.root, className)}>
      <span className={classes.score}>
        <hr />
      </span>
      {content && <span className={classes.content}>{content}</span>}
      <span className={classes.score}>
        <hr />
      </span>
    </div>
  );
};

export default Hr;
