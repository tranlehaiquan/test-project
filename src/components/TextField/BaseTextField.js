import React from 'react';
import makeStyle from '@material-ui/core/styles/makeStyles';
import TextField from '@material-ui/core/TextField';
import red from '@material-ui/core/colors/red';

const useStyles = makeStyle(({ spacing }) => ({
  root: {},
  label: {
    fontWeight: 700,
    marginBottom: spacing(1),
    display: 'block',
    color: '#4A4854',
    fontSize: 16,
  },
  requiredNotice: {
    color: red.A400,
  },
}));

const TextFieldCustom = ({ label, variant = 'outlined', ...restProps }) => {
  const { required } = restProps;
  const classes = useStyles();

  return (
    <>
      <label className={classes.label}>
        {label} {required && <span className={classes.requiredNotice}>*</span>}
      </label>
      <TextField {...restProps} variant={variant} />
    </>
  );
};

export default TextFieldCustom;
