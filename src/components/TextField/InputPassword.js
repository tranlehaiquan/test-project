import React from 'react';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import IconButton from '@material-ui/core/IconButton';

import BaseTextField from './BaseTextField';

const InputPasswordHideShow = ({
  initialShowPassword = false,
  ...restProps
}) => {
  const [showPassword, setShowPassword] = React.useState(initialShowPassword);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const endAdornment = (
    <InputAdornment position="end">
      <IconButton
        aria-label="toggle password visibility"
        onClick={handleClickShowPassword}
        onMouseDown={handleMouseDownPassword}
        edge="end"
      >
        {showPassword ? (
          <Visibility />
        ) : (
          <Visibility style={{ opacity: 0.5 }} />
        )}
      </IconButton>
    </InputAdornment>
  );
  const type = showPassword ? 'text' : 'password';

  return (
    <BaseTextField type={type} InputProps={{ endAdornment }} {...restProps} />
  );
};

export default InputPasswordHideShow;
