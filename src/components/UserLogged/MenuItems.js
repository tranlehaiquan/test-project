import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import { useDispatch } from 'react-redux';

import { logout } from 'actions/user';

function MenuItems() {
  const dispatch = useDispatch();

  const handleLogout = (event) => {
    dispatch(logout());
  };

  return (
    <>
      <MenuItem onClick={() => {}}>Profile</MenuItem>
      <MenuItem onClick={handleLogout}>Logout</MenuItem>
    </>
  );
}

export default MenuItems;
