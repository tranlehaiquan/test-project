import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

/**
 * Everything style base button in here
 * it can be inherit base custom button like PrimaryButton...
 */
const BaseButton = withStyles(({ palette }) => ({
  root: {
    textTransform: 'none',
    fontSize: 16,
    lineHeight: 1.5,
  },
}))(Button);

export default BaseButton;
