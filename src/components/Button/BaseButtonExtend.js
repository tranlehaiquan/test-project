import React from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from './BaseButton';
import cls from 'clsx';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
    display: 'inline-block',
    textTransform: 'none',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  fullWidth: {
    display: 'block',
  },
}));

const BaseButton = ({
  loading = false,
  fullWidth,
  disableElevation = true,
  btnRef,
  ...rest
}) => {
  const classes = useStyles();

  return (
    <div className={cls(classes.root, fullWidth && classes.fullWidth)}>
      <Button
        {...rest}
        fullWidth={fullWidth}
        ref={btnRef}
        disableElevation={disableElevation}
      />
      {loading && (
        <CircularProgress size={24} className={classes.buttonProgress} />
      )}
    </div>
  );
};

export default React.forwardRef((props, ref) => (
  <BaseButton btnRef={ref} {...props} />
));
