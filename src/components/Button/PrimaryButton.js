import { withStyles } from '@material-ui/core/styles';
import BaseButtonExtend from './BaseButtonExtend';

const PrimaryButton = withStyles(({ palette }) => ({
  root: {
    boxShadow: 'none',
    fontSize: 16,
    padding: '6px 12px',
    border: '1px solid',
    lineHeight: 1.5,
    backgroundColor: palette.primary.main,
    borderColor: palette.primary.main,
    color: 'white',
    '&:hover': {
      backgroundColor: palette.primary.main,
      borderColor: palette.primary.main,
      boxShadow: 'none',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: palette.primary.main,
      borderColor: palette.primary.main,
    },
    '&:focus': {
      boxShadow: `0 0 0 0.2rem ${palette.primary.main}`,
    },
    '&:disabled': {
      borderColor: palette.common.white,
      boxShadow: 'none',
      backgroundColor: palette.common.white,
      color: 'rgba(0, 0, 0, 0.4)',
      '&:hover': {
        backgroundColor: palette.common.white,
      },
      '&:active': {
        backgroundColor: palette.common.white,
      },
      '&:focus': {
        backgroundColor: palette.common.white,
      },
    },
  },
}))(BaseButtonExtend);

export default PrimaryButton;
