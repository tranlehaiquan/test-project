import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';

import Copyright from './Copyright';
import QuickLinks from './QuickLinks';

const useStyles = makeStyles(({ spacing, palette }) => ({
  footer: {
    backgroundColor: palette.background.paper,
  },
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <Hidden smDown>
        <QuickLinks />
      </Hidden>
      <Copyright />
    </footer>
  );
}
