import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(({ palette, spacing, breakpoints }) => ({
  root: {
    borderTop: `1px solid ${  palette.grey[300]}`,
    paddingTop: spacing(1),
    paddingBottom: spacing(1),
  },
  socials: {
    textAlign: 'right',
    [breakpoints.down('sm')]: {
      textAlign: 'center',
      marginTop: spacing(1),
    },
  },
  socialIcon: {
    fill: '#969cae',
    paddingLeft: spacing(1),
    paddingRight: spacing(1),
    '&:hover': {
      fill: palette.primary.main,
    },
  },
}));
