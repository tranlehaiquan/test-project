import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import Container from '../Container';
import styles from './Footer.scss';
import useStyles from './CopyrightStyles';

function Copyright() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container spacing={1}>
        <Grid container>
          <Grid item sm={8} xs={12}>
            <Typography>
              © 2019, Sqwift Stars (An RR Ventures Company). All Rights
              Generally Reserved.
            </Typography>
          </Grid>
          <Grid item sm={4} xs={12} className={classes.socials}>
            <Box component="span" m={1} color="text.secondary">
              <a
                href="https://www.facebook.com/sqwiftstars/"
                target="_blank"
                rel="noopener noreferrer"
                className={classes.socialIcon}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  id="facebook_4_"
                  width="8.744"
                  height="17.31"
                  data-name="facebook (4)"
                  viewBox="0 0 8.744 17.31"
                >
                  <path
                    id="XMLID_835_"
                    d="M76.982 9.219h1.9v7.811a.279.279 0 0 0 .279.279h3.217a.279.279 0 0 0 .279-.279V9.256h2.181a.279.279 0 0 0 .277-.247l.331-2.876a.279.279 0 0 0-.277-.311h-2.514v-1.8c0-.543.293-.819.87-.819h1.643a.279.279 0 0 0 .279-.279V.281A.279.279 0 0 0 85.168 0H82.8a4.338 4.338 0 0 0-2.837 1.069 2.975 2.975 0 0 0-.989 2.644v2.109h-1.992a.279.279 0 0 0-.282.278v2.84a.279.279 0 0 0 .282.279z"
                    transform="translate(-76.703)"
                  />
                </svg>
              </a>
            </Box>

            <Box component="span" m={1}>
              <a
                className={classes.socialIcon}
                href="https://twitter.com/sqwift"
                target="_blank"
                rel="noopener noreferrer"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="17.994"
                  height="14.616"
                  viewBox="0 0 17.994 14.616"
                >
                  <g id="twitter-logo-silhouette">
                    <g id="Group_2" data-name="Group 2">
                      <path
                        id="Path_1"
                        d="M17.995 59.17a7.371 7.371 0 0 1-2.119.581 3.707 3.707 0 0 0 1.624-2.04 7.441 7.441 0 0 1-2.345.9 3.694 3.694 0 0 0-6.29 3.365 10.48 10.48 0 0 1-7.608-3.857A3.7 3.7 0 0 0 2.4 63.043a3.692 3.692 0 0 1-1.672-.463v.046a3.7 3.7 0 0 0 2.961 3.619 3.739 3.739 0 0 1-.973.129 3.544 3.544 0 0 1-.695-.069 3.693 3.693 0 0 0 3.447 2.564 7.406 7.406 0 0 1-4.587 1.578A7.844 7.844 0 0 1 0 70.4a10.434 10.434 0 0 0 5.658 1.661 10.43 10.43 0 0 0 10.5-10.5l-.012-.478a7.371 7.371 0 0 0 1.849-1.913z"
                        data-name="Path 1"
                        transform="translate(-.001 -57.441)"
                      />
                    </g>
                  </g>
                </svg>
              </a>
            </Box>

            <Box component="span" m={1}>
              <a
                className={classes.socialIcon}
                href="https://www.linkedin.com/company/sqwift"
                target="_blank"
                rel="noopener noreferrer"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  id="Group_7222"
                  width="15.113"
                  height="14.886"
                  data-name="Group 7222"
                  viewBox="0 0 15.113 14.886"
                  className={styles.socialIcon}
                >
                  <path
                    id="Path_10131"
                    d="M101.355 204.5h3.18v9.858h-3.18zm0 0"
                    data-name="Path 10131"
                    transform="translate(-101.052 -199.477)"
                  />
                  <path
                    id="Path_10132"
                    d="M216.374 199.98a4.127 4.127 0 0 0-3.612 1.689v-1.477h-2.93v9.858h3.18v-4.95c0-1.431.659-2.544 1.908-2.544s1.454 1.385 1.454 3.089v4.407h3.18v-6.89a3.18 3.18 0 0 0-3.18-3.18zm0 0"
                    data-name="Path 10132"
                    transform="translate(-204.441 -195.165)"
                  />
                  <path
                    id="Path_10133"
                    d="M98.68 99.21a1.893 1.893 0 1 1-1.893-1.893 1.893 1.893 0 0 1 1.893 1.893zm0 0"
                    data-name="Path 10133"
                    transform="translate(-94.894 -97.317)"
                  />
                </svg>
              </a>
            </Box>

            <Box component="span" m={1}>
              <a
                className={classes.socialIcon}
                href="https://www.instagram.com/sqwiftstars/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  id="instagram_1_"
                  width="16.991"
                  height="16.991"
                  data-name="instagram (1)"
                  viewBox="0 0 16.991 16.991"
                >
                  <g id="Group_4" data-name="Group 4">
                    <g id="Group_3" data-name="Group 3">
                      <path
                        id="Path_2"
                        d="M11.682 0H5.31A5.31 5.31 0 0 0 0 5.31v6.372a5.31 5.31 0 0 0 5.31 5.31h6.372a5.31 5.31 0 0 0 5.31-5.31V5.31A5.31 5.31 0 0 0 11.682 0zM15.4 11.682a3.721 3.721 0 0 1-3.718 3.718H5.31a3.721 3.721 0 0 1-3.717-3.717V5.31A3.721 3.721 0 0 1 5.31 1.593h6.372A3.721 3.721 0 0 1 15.4 5.31z"
                        data-name="Path 2"
                      />
                    </g>
                  </g>
                  <g
                    id="Group_6"
                    data-name="Group 6"
                    transform="translate(4.248 4.248)"
                  >
                    <g id="Group_5" data-name="Group 5">
                      <path
                        id="Path_3"
                        d="M132.248 128a4.248 4.248 0 1 0 4.248 4.248 4.248 4.248 0 0 0-4.248-4.248zm0 6.9a2.655 2.655 0 1 1 2.655-2.655 2.659 2.659 0 0 1-2.655 2.655z"
                        data-name="Path 3"
                        transform="translate(-128 -128)"
                      />
                    </g>
                  </g>
                  <g
                    id="Group_8"
                    data-name="Group 8"
                    transform="translate(12.496 3.363)"
                  >
                    <g id="Group_7" data-name="Group 7">
                      <circle
                        id="Ellipse_1"
                        cx=".566"
                        cy=".566"
                        r=".566"
                        data-name="Ellipse 1"
                      />
                    </g>
                  </g>
                </svg>
              </a>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default Copyright;
