import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(({ spacing, palette, typography }) => ({
  footerLogo: {
    maxWidth: '100%',
    display: 'block',
    '& img': {
      maxHeight: 36,
    },
  },
  root: {
    padding: spacing(2, 0),
  },
  quickLinksTitle: {
    fontSize: typography.pxToRem(16),
    fontWeight: 700,
  },
  quickLink: {
    color: palette.common.black,
    fontSize: typography.pxToRem(12),
    fontWeight: 400,
    padding: spacing(0.1, 0),
    display: 'inline-block',
  },
}));
