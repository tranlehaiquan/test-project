import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
// import footerlogo from 'assets/footerlogo.png';
// import footerlogoWebp from 'assets/footerlogo.webp';
import Link, { LinkBase } from 'components/Link';

import useStyles from './QuickLinksStyles';
import Container from '../Container';

const LINKS = [
  {
    label: 'FAQ',
    to: '/faq',
  },
  {
    label: 'Terms',
    to: 'http://sqwift.com/terms-of-use/',
  },
  {
    label: 'Support',
    to: 'mailto:support@sqwift.com',
  },
  {
    label: 'Privacy Policy',
    to: 'http://sqwift.com/privacy-policy/',
  },
];

export default function QuickLinksTitle() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container>
        <Grid container spacing={1} justify="space-between" alignItems="center">
          <Grid item sm={7}>
            <Typography variant="subtitle2">
              Sqwift Stars is acutely tailored to create the most personalized
              and authentic fan experiences, one-on-one without the intervention
              of PR managers or agents to create unique moments.
            </Typography>
          </Grid>
          <Grid item sm={3}>
            <Typography variant="h5" className={classes.quickLinksTitle}>
              Quick Links
            </Typography>
            <Grid container>
              {LINKS.map((link) => (
                <Grid item xs={6} key={link.label}>
                  {link.to.startsWith('/') ? (
                    <Link to={link.to} className={classes.quickLink}>
                      {link.label}
                    </Link>
                  ) : (
                    <LinkBase href={link.to} className={classes.quickLink}>
                      {link.label}
                    </LinkBase>
                  )}
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
