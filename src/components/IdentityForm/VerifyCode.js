import React from 'react';
import { useFormik } from 'formik';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import Typo from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useHistory } from 'react-router-dom';
import useQuery from 'utils/useQuery';

import { PrimaryButton } from 'components/Button';
import getValidationSchema from 'utils/validation';
import TextField from 'components/TextField';
import { userVerifyForgotCode } from 'actions/user';
import { useDispatch } from 'react-redux';
import pick from 'lodash/pick';

const useStyles = makeStyles(({ spacing, palette }) => ({
  inputWrapper: {
    paddingBottom: spacing(2),
  },
}));

export default function VerifyCode() {
  const classes = useStyles();
  const history = useHistory();
  const query = useQuery();
  const email =
    query.get('email') ||
    (history.location.state ? pick(history.location.state, 'email') : '');
  const dispatch = useDispatch();
  const [pending, setPending] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      code: '',
    },
    validationSchema: getValidationSchema(['code']),
    onSubmit: async (values) => {
      setPending(true);
      const result = await dispatch(
        userVerifyForgotCode({
          email,
          code: values.code,
        })
      );
      if (!result) setPending(false);
    },
  });

  // make sure email not empty
  if (!email) history.push('/');

  return (
    <div className={classes.root}>
      <Box mb={2}>
        <Typo variant="h5">Verify code</Typo>
        <Typo>
          Code send to your mail <strong>{email}</strong>. Please fill out to
          reset your password.
        </Typo>
      </Box>
      <div className={classes.inputWrapper}>
        <TextField
          value={formik.values.code}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="code"
          name="code"
          className={classes.input}
          fullWidth
          label="Your code"
          error={Boolean(formik.touched.code && formik.errors.code)}
          helperText={formik.touched.code ? formik.errors.code : ''}
          disabled={pending}
        />
      </div>
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={12}>
          <PrimaryButton
            fullWidth
            variant="contained"
            type="submit"
            size="medium"
            onClick={() => {
              formik.handleSubmit();
            }}
            disabled={pending}
            loading={pending}
          >
            Verify
          </PrimaryButton>
        </Grid>
      </Grid>
    </div>
  );
}
