export { default as Login } from './Login';
export { default as SignUp } from './SignUp';
export { default as ForgotPwd } from './ForgotPwd';
export { default as VerifyCode } from './VerifyCode';
export { default as ResetPwd } from './ResetPwd';
