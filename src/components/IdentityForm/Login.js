import React from 'react';
import { useFormik } from 'formik';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import Typo from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { PrimaryButton } from 'components/Button';
import getValidationSchema from 'utils/validation';
import TextField, { InputPassword } from 'components/TextField';
import Link from 'components/Link';
import Hr from 'components/Hr';
import { login } from 'actions/user';
import { useDispatch } from 'react-redux';

import { SOCIALS } from './constants';

const useStyles = makeStyles(({ spacing }) => ({
  root: {},
  input: {},
  title: {
    marginBottom: spacing(4),
  },
  inputWrapper: {
    paddingBottom: spacing(2),
  },
  hr: {
    marginTop: spacing(3),
    marginBottom: spacing(2),
  },
  socials: {
    marginBottom: spacing(2),
  },
  social: {
    boxShadow: '0 3px 7px 0 rgba(0, 0, 0, 0.07)',
    backgroundColor: '#ffffff',
    height: 50,
    width: 50,
    borderRadius: '50%',
    margin: spacing(0, 2),
  },
  signup: {
    textAlign: 'center',
    paddingTop: spacing(2),
  },
  signupLink: {
    fontWeight: '700',
  },
}));

export default function LoginForm({ open, handleClose }) {
  const dispatch = useDispatch();
  const [pending, setPending] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validationSchema: getValidationSchema(['email', 'password']),
    onSubmit: async (values) => {
      setPending(true);
      const result = await dispatch(login(values));
      if (!result) setPending(false);
    },
  });
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typo variant="h5" className={classes.title}>
        Login to Your Account
      </Typo>
      <div className={classes.inputWrapper}>
        <TextField
          value={formik.values.email}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="email"
          name="email"
          className={classes.input}
          fullWidth
          label="Email"
          error={Boolean(formik.touched.email && formik.errors.email)}
          helperText={formik.touched.email ? formik.errors.email : ''}
          disabled={pending}
        />
      </div>
      <div className={classes.inputWrapper}>
        <InputPassword
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="password"
          name="password"
          className={classes.input}
          fullWidth
          label="Password"
          error={Boolean(formik.touched.password && formik.errors.password)}
          helperText={formik.touched.password ? formik.errors.password : ''}
          disabled={pending}
        />
      </div>
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={6}>
          <PrimaryButton
            fullWidth
            variant="contained"
            type="submit"
            size="medium"
            onClick={() => {
              formik.handleSubmit();
            }}
            disabled={pending}
            loading={pending}
          >
            Login
          </PrimaryButton>
        </Grid>
        <Grid item xs={6}>
          <Box textAlign="center">
            <Link to="/forgot-password">Forgot password</Link>
          </Box>
        </Grid>
      </Grid>
      <Hr className={classes.hr} content="Or Connect with" />
      <Box display="flex" justifyContent="center" className={classes.socials}>
        {SOCIALS.map((social) => (
          <Box
            key={social.icon}
            className={classes.social}
            display="inline-flex"
            justifyContent="center"
            alignItems="center"
          >
            <img src={social.icon} alt={social.title} />
          </Box>
        ))}
      </Box>
      <Typo className={classes.signup}>
        Don't have an account?{' '}
        <Link to="/register" className={classes.signupLink}>
          Signup
        </Link>
      </Typo>
    </div>
  );
}
