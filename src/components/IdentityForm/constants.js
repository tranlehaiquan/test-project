import fbIcon from 'assets/socials/facebook.png';
import googleIcon from 'assets/socials/google.jpg';
import instagram from 'assets/socials/instagram.png';

export const SOCIALS = [
  {
    icon: fbIcon,
    title: 'Facebook',
  },
  {
    icon: googleIcon,
    title: 'Google',
  },
  {
    icon: instagram,
    title: 'Instagram',
  },
];
