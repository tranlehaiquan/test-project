import React from 'react';
import { useFormik } from 'formik';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import Typo from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import { PrimaryButton } from 'components/Button';
import getValidationSchema from 'utils/validation';
import TextField from 'components/TextField';
import Link from 'components/Link';
import { userForgotPwd } from 'actions/user';
import { useDispatch } from 'react-redux';

const useStyles = makeStyles(({ spacing, palette }) => ({
  inputWrapper: {
    paddingBottom: spacing(2),
  },
  signInLink: {
    color: palette.primary.main,
    fontWeight: 800,
  },
}));

export default function ForgotForm() {
  const dispatch = useDispatch();
  const [pending, setPending] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema: getValidationSchema(['email']),
    onSubmit: async (values) => {
      setPending(true);
      const result = await dispatch(userForgotPwd(values.email));
      if (!result) setPending(false);
    },
  });
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Box mb={2}>
        <Typo variant="h5">Forgot your Password?</Typo>
        <Typo>
          Enter your email address and we'll send you a code to reset your
          password.
        </Typo>
      </Box>
      <div className={classes.inputWrapper}>
        <TextField
          value={formik.values.email}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="email"
          name="email"
          className={classes.input}
          fullWidth
          label="Email"
          error={Boolean(formik.touched.email && formik.errors.email)}
          helperText={formik.touched.email ? formik.errors.email : ''}
          disabled={pending}
        />
      </div>
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={6}>
          <PrimaryButton
            fullWidth
            variant="contained"
            type="submit"
            size="medium"
            onClick={() => {
              formik.handleSubmit();
            }}
            disabled={pending}
            loading={pending}
          >
            Reset password
          </PrimaryButton>
        </Grid>
        <Grid item xs={6}>
          <Box textAlign="center">
            <Typo>Already a member? </Typo>
            <Link to="/login" className={classes.signInLink}>
              Sign in
            </Link>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}
