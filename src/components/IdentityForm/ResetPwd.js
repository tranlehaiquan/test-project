import React from 'react';
import { useFormik } from 'formik';
import makeStyles from '@material-ui/core/styles/makeStyles';
import Grid from '@material-ui/core/Grid';
import Typo from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import pick from 'lodash/pick';

import { PrimaryButton } from 'components/Button';
import getValidationSchema from 'utils/validation';
import { InputPassword } from 'components/TextField';
import { resetForgotPassword } from 'actions/user';
import useQuery from 'utils/useQuery';

const useStyles = makeStyles(({ spacing, palette }) => ({
  inputWrapper: {
    paddingBottom: spacing(2),
  },
}));

export default function ResetPwd() {
  const classes = useStyles();
  const history = useHistory();
  const query = useQuery();
  const email =
    query.get('email') ||
    (history.location.state ? pick(history.location.state, 'email') : '');
  const code =
    query.get('code') ||
    (history.location.state ? pick(history.location.state, 'code') : '');
  const dispatch = useDispatch();
  const [pending, setPending] = React.useState(false);
  const formik = useFormik({
    initialValues: {
      password: '',
      passwordConfirm: '',
    },
    validationSchema: getValidationSchema(['password', 'passwordConfirm']),
    onSubmit: async (values) => {
      setPending(true);
      const result = await dispatch(
        resetForgotPassword({
          email,
          code,
          password: values.password,
        })
      );
      if (!result) setPending(false);
    },
  });

  // make sure email and code not empty
  if (!email || !code) history.push('/');

  return (
    <div className={classes.root}>
      <Box mb={2}>
        <Typo variant="h5">Reset your password</Typo>
      </Box>
      <div className={classes.inputWrapper}>
        <InputPassword
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="password"
          name="password"
          className={classes.input}
          fullWidth
          label="New Password"
          error={Boolean(formik.touched.password && formik.errors.password)}
          helperText={formik.touched.password ? formik.errors.password : ''}
          disabled={pending}
        />
      </div>
      <div className={classes.inputWrapper}>
        <InputPassword
          value={formik.values.passwordConfirm}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          id="passwordConfirm"
          name="passwordConfirm"
          className={classes.input}
          fullWidth
          label="New Password"
          error={Boolean(
            formik.touched.passwordConfirm && formik.errors.passwordConfirm
          )}
          helperText={
            formik.touched.passwordConfirm ? formik.errors.passwordConfirm : ''
          }
          disabled={pending}
        />
      </div>
      <Grid container spacing={0} alignItems="center">
        <Grid item xs={12}>
          <PrimaryButton
            fullWidth
            variant="contained"
            type="submit"
            size="medium"
            onClick={() => {
              formik.handleSubmit();
            }}
            disabled={pending}
            loading={pending}
          >
            Reset password
          </PrimaryButton>
        </Grid>
      </Grid>
    </div>
  );
}
