import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Link from './LinkBase';

const LinkBehavior = React.forwardRef((props, ref) => {
  const { to, ...restProps } = props;
  return <RouterLink ref={ref} to={to} {...restProps} />;
});

/**
 * Custom Link with behavior of Link react router
 * and style of material ui
 */
const CustomLink = ({ children, ...restProps }) => {
  return (
    <Link component={LinkBehavior} {...restProps}>
      {children}
    </Link>
  );
};

export default CustomLink;
