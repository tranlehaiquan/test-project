import React from 'react';
import Link from '@material-ui/core/Link';
import makeStyles from '@material-ui/core/styles/makeStyles';
import cls from 'clsx';

const useStyles = makeStyles(({ palette }) => ({
  root: {
    color: palette.common.black,
  },
}));

const LinkBase = ({ underline = 'none', linkRef, className, ...restProps }) => {
  const classes = useStyles();

  return (
    <Link
      ref={linkRef}
      underline={underline}
      {...restProps}
      className={cls(classes.root, className)}
    />
  );
};

export default React.forwardRef((props, ref) => (
  <LinkBase linkRef={ref} {...props} />
));
