import React from 'react';
import Container from '@material-ui/core/Container';

const CustomContainer = ({ children, ...restProps }) => {
  return (
    <Container {...restProps} maxWidth="md">
      {children}
    </Container>
  );
};

export default CustomContainer;
