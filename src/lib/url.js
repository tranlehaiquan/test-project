// @flow

import * as R from 'ramda';

const queryPair = (name, value) => {
  if (typeof value === 'undefined') {
    return undefined;
  }
  return `${name}=${encodeURIComponent(value)}`;
};

const queryString = (opts: ?Object) => {
  const options = opts || {};
  const result = R.pipe(
    R.map(key => {
      const value = options[key];
      if (Array.isArray(value)) {
        return value.map(v => queryPair(key, v));
      }
      return queryPair(key, value);
    }),
    R.flatten,
    R.join('&'),
  )(Object.keys(options));
  return result ? `?${result}` : '';
};

function appendQueryString(url: string, query: string): string {
  const joiner: string = url.includes('?') ? '&' : '?';
  return query ? url + joiner + query : url;
}

function appendForCompanyParam(url: string, forCompanyId: number): string {
  return appendQueryString(url, `forCompany=${forCompanyId}`);
}

function forwardForCompanyParam<T, U, V, W, X>(
  getQueryString: () => string,
  urlFactory: (T, U, V, W, X) => string,
): (T, U, V, W, X) => string {
  return (...args) => {
    const allParams: string[] = getQueryString().split('&');
    const forCompanyParam: string | void = allParams.find(p =>
      p.includes('forCompany='),
    );
    const url: string = urlFactory(...args);
    return forCompanyParam ? appendQueryString(url, forCompanyParam) : url;
  };
}

export { queryString, appendForCompanyParam, forwardForCompanyParam };
