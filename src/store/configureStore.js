import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import rootReducer from '../reducers';

const persistConfig = {
  key: 'sqwift',
  storage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const history = createBrowserHistory();

const logger = createLogger({ collapsed: true });
const isDev = process.env.NODE_ENV === 'development';

function configureStore(preloadedState) {
  const middlewares = isDev
    ? compose(
        applyMiddleware(
          thunk,
          logger
          // ... other middlewares ...
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__
          ? window.__REDUX_DEVTOOLS_EXTENSION__()
          : (f) => f
      )
    : applyMiddleware(thunk);

  const store = createStore(persistedReducer, preloadedState, middlewares);

  return store;
}
export const store = configureStore();
export const persistor = persistStore(store);
