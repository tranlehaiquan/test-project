// @flow

// In deployed environments we will have a global ENVIRONMENT variable available.
// See /templates/index.html

// The values within the global ENVIRONMENT variable are provided by the deploy script.
// See /scripts/deploy.js

import environmentConfigs from './environments.json';
import * as R from 'ramda';

const fallbackEnvId = 'dev';
const envId = R.defaultTo(
  fallbackEnvId,
  (typeof ENVIRONMENT === 'object' ? ENVIRONMENT : process.env).ENVIRONMENT_ID // eslint-disable-line
);
const targetEnvConfig = environmentConfigs[envId];

if (!targetEnvConfig) {
  throw new Error(`Environment config not found for ${envId}`);
}

export default {
  apiUrl: targetEnvConfig.apiUrl,
  firebase: {
    apiKey: targetEnvConfig.apiKey,
    authDomain: targetEnvConfig.authDomain,
    databaseURL: targetEnvConfig.databaseURL,
    projectId: targetEnvConfig.projectId,
    storageBucket: targetEnvConfig.storageBucket,
    messagingSenderId: targetEnvConfig.messagingSenderId,
    appId: targetEnvConfig.appId,
    measurementId: targetEnvConfig.measurementId,
  },
};
