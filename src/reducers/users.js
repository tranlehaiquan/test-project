import { ACTIONS } from 'actions/user';

const defaultState = {
  user: null,
  token: null,
};

export default function users(state = defaultState, action) {
  switch (action.type) {
    case ACTIONS.SET_USER_LOGIN: {
      return {
        ...state,
        user: {
          ...action.payload.user,
        },
      };
    }
    case ACTIONS.CHANGE_TOKEN_LOGIN: {
      return {
        ...state,
        token: action.payload.token,
      };
    }
    case ACTIONS.REMOVE_USER_LOGIN: {
      return defaultState;
    }
    default:
      return state;
  }
}
