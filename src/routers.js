import React from 'react';
import { Router } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import history from 'customHistory';

import Login from 'pages/Login';
import SignUp from 'pages/SignUp';
import Home from 'pages/Home';
import ForgotPwd from 'pages/ForgotPwd';
import VerifyCodeForgotPwd from 'pages/VerifyCodeForgotPwd';
import ResetPwd from 'pages/ResetPwd';

export default function Routes() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/register" component={SignUp} />
        <Route path="/forgot-password" component={ForgotPwd} />
        <Route path="/verify-code" component={VerifyCodeForgotPwd} />
        <Route path="/reset-password" component={ResetPwd} />
      </Switch>
    </Router>
  );
}
