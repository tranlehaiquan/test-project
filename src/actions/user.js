import api from 'services/api';
import history from 'customHistory';
import Snackbar from 'components/SnackbarUtilsConfigurator';

export const ACTIONS = {
  SET_USER_LOGIN: 'SET_USER_LOGIN',
  REMOVE_USER_LOGIN: 'REMOVE_USER_LOGIN',
  CHANGE_TOKEN_LOGIN: 'CHANGE_TOKEN_LOGIN',
};

/**
 *
 * @param {Object} user
 */
const setUser = (user) => ({
  type: ACTIONS.SET_USER_LOGIN,
  payload: {
    user,
  },
});

const removeUser = () => ({
  type: ACTIONS.REMOVE_USER_LOGIN,
});

/**
 *
 * @param {string} token
 */
const changeToken = (token) => ({
  type: ACTIONS.CHANGE_TOKEN_LOGIN,
  payload: {
    token,
  },
});

/**
 *
 * @param {email: string, password: string} userInfo
 * @returns {Promise<boolean>}
 */
export const login = (userInfo) => async (dispatch) => {
  try {
    const userRes = await api.login(userInfo);
    dispatch(setUser(userRes.user));
    dispatch(changeToken(userRes.token));
    history.push('/');
    Snackbar.success(userRes.message);
    return true;
  } catch (err) {
    if (err.message) Snackbar.error(err.message);
    return false;
  }
};

/**
 *
 * @param {email: string, password: string} userInfo
 * @returns {Promise<boolean>}
 */
export const userRegister = (userInfo) => async (dispatch) => {
  try {
    const userRes = await api.register(userInfo);
    dispatch(setUser(userRes.user));
    dispatch(changeToken(userRes.token));
    history.push('/');
    Snackbar.success(userRes.message);
    return true;
  } catch (err) {
    if (err.message) Snackbar.error(err.message);
    return false;
  }
};

/**
 * @param {string} email
 * @return {Promise<boolean>}
 */
export const userForgotPwd = (email) => async (dispatch) => {
  try {
    const userRes = await api.forgotPassword(email);
    Snackbar.success(userRes.message);
    history.push(`/verify-code?email=${email}`);
    return true;
  } catch (err) {
    if (err.message) Snackbar.error(err.message);
    return false;
  }
};

/**
 * @param {code: string, email: string} userInfo
 * @return {Promise<boolean>}
 */
export const userVerifyForgotCode = (userInfo) => async (dispatch) => {
  try {
    const verifyResponse = await api.verifyCodeForgotPwd(userInfo);
    Snackbar.success(verifyResponse.message);
    history.push(
      `/reset-password?code=${userInfo.code}&email=${userInfo.email}`
    );
    return true;
  } catch (err) {
    if (err.message) Snackbar.error(err.message);
    return false;
  }
};

/**
 * @param {code: string, email: string, password: string} userInfo
 * @return {Promise<boolean>}
 */
export const resetForgotPassword = (userInfo) => async (dispatch) => {
  try {
    const verifyResponse = await api.resetPasswordForgotten(userInfo);
    Snackbar.success(verifyResponse.message);
    history.push('/login');
    return true;
  } catch (err) {
    if (err.message) Snackbar.error(err.message);
    return false;
  }
};

export const logout = () => async (dispatch) => {
  api.logout();
  dispatch(removeUser());
};
